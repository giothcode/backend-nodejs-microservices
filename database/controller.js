const { ObjectId } = require("mongodb");

const { MongoDB } = require("./mongo");

module.exports.DatabaseController = (COLLECTION) => {
  const list = async () => {
    const collection = await MongoDB(COLLECTION);
    return collection.find().toArray();
  };

  const getById = async (id) => {
    const collection = await MongoDB(COLLECTION);
    return collection.findOne({ _id: ObjectId(id) });
  };

  const create = async (document) => {
    const collection = await MongoDB(COLLECTION);
    const result = await collection.insertOne(document);
    return { insertedId: result.insertedId, document };
  };

  const update = async (id, query) => {
    const collection = await MongoDB(COLLECTION);
    query = toID(query);
    const result = await collection.updateOne({ _id: ObjectId(id) }, query);
    return {
      matchedCount: result.matchedCount,
      modifiedCount: result.modifiedCount,
    };
  };

  const deleteDoc = async (id) => {
    const collection = await MongoDB(COLLECTION);
    const result = await collection.deleteOne({ _id: ObjectId(id) });
    return { deletedCount: result.deletedCount };
  };

  const deleteArray = async (target, idsArray) => {
    const targetCollection = await MongoDB(target);
    idsArray = idsArray.map((id) => ObjectId(id));
    const result = await targetCollection.deleteMany({
      _id: { $in: idsArray },
    });
    return { nRemoved: result.nRemoved };
  };

  const aggregate = async (query) => {
    query = query.map((chunk) => toID(chunk));
    const collection = await MongoDB(COLLECTION);
    return await collection.aggregate(query).toArray();
  };

  const toID = (object) => {
    for (const key in object) {
      if (
        typeof object[key] === "string" &&
        (key === "_id" || key.includes("_id"))
      ) {
        object[key] = ObjectId(object[key]);
      }
      if (typeof object[key] === "object") {
        toID(object[key]);
      }
    }
    return object;
  };

  return { list, getById, create, update, deleteDoc, aggregate, deleteArray };
};
