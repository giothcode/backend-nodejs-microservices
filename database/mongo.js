const { MongoClient } = require("mongodb");
const debug = require("debug")("service:database-connection");

const {
  database: { mongo },
} = require("../config");

const DB_URI = mongo.uri;
var connection = null;

module.exports.MongoDB = async (collection) => {
  if (!connection) {
    const client = new MongoClient(DB_URI);
    connection = await client.connect();
    debug("New mongo connection");
  } else debug("Reconnected with mongo");

  const db = connection.db();
  return db.collection(collection);
};
