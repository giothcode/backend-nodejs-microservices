const express = require("express");
const debug = require("debug")("service:database");

const { DatabaseNetwork } = require("./network");
const { ErrorMiddleware } = require("../network/errors");

const {
  database: { port: PORT },
} = require("../config");

const app = express();

app.use(express.json());

app.use("/", DatabaseNetwork);
app.use(ErrorMiddleware);

app.listen(PORT, () => {
  debug(`Escuchando en el puerto ${PORT}`);
});
