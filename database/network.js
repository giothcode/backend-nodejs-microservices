const router = require("express").Router();

const response = require("../network/response");
const { DatabaseController } = require("./controller");

router
  .get("/:collection", async (req, res, next) => {
    try {
      const {
        params: { collection },
      } = req;
      const documents = await DatabaseController(collection).list();
      response.success(res, 200, `Lista ${collection}`, documents);
    } catch (error) {
      next(error);
    }
  })
  .get("/:collection/:id", async (req, res, next) => {
    try {
      const {
        params: { collection, id },
      } = req;
      const document = await DatabaseController(collection).getById(id);
      response.success(res, 200, `Documento ${collection} - ${id}`, document);
    } catch (error) {
      next(error);
    }
  })
  .post("/:collection", async (req, res, next) => {
    try {
      const {
        params: { collection },
        body,
      } = req;
      const document = await DatabaseController(collection).create(body);
      response.success(res, 201, `Documento creado en ${collection}`, document);
    } catch (error) {
      next(error);
    }
  })
  .post("/:collection/aggregate", async (req, res, next) => {
    try {
      const {
        params: { collection },
        body,
      } = req;
      const documents = await DatabaseController(collection).aggregate(body);
      response.success(
        res,
        201,
        `Resultado aggregate en ${collection}`,
        documents
      );
    } catch (error) {
      next(error);
    }
  })
  .patch("/:collection/:id", async (req, res, next) => {
    try {
      const {
        params: { collection, id },
        body,
      } = req;
      const document = await DatabaseController(collection).update(id, body);
      response.success(
        res,
        200,
        `Documento actualizado en ${collection}`,
        document
      );
    } catch (error) {
      next(error);
    }
  })
  .delete("/:collection/to/:target", async (req, res, next) => {
    try {
      const {
        params: { collection, target },
        body,
      } = req;
      const result = await DatabaseController(collection).deleteArray(
        target,
        body
      );
      response.success(
        res,
        200,
        `Documentos eliminado en ${collection}`,
        result
      );
    } catch (error) {
      next(error);
    }
  })
  .delete("/:collection/:id", async (req, res, next) => {
    try {
      const {
        params: { collection, id },
      } = req;
      const result = await DatabaseController(collection).deleteDoc(id);
      response.success(
        res,
        200,
        `Documento eliminado en ${collection}`,
        result
      );
    } catch (error) {
      next(error);
    }
  });

module.exports.DatabaseNetwork = router;
