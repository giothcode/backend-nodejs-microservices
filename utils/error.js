module.exports.CreateError = (code, message) => {
  let error = new Error(message);
  error.statusCode = code;
  return error;
};
