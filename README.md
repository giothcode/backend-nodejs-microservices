# Prueba backend

## Task 1

### Objetivo:

> Construir un API que permita gestionar una base de datos de usuarios y sus respectivas redes sociales.

### Requerimientos funcionales

#### El API debe permitir:

1. Consultar la lista de todos los usuarios.
2. Consultar un usuario específico con la lista de todas sus redes sociales como nombre de la red, username, enlace, ícono.

### Requerimientos técnicos

Stack: `Node.js`, `Express`, `MongoDB`

#### Publicación:

1. Se debe de entregar el API desplegada en algún servicio como AWS, Digital Ocean, etc. La idea es poder probar el funcionamiento.
2. Se debe de entregar documentación de la API en servicios como Postman o alguno similar de libre elección.

#### Bonus

> Construir un frontend en cualquier tecnología para el consumo del API.

# Solución

Se construyo 3 servicios:

1. \***\*API\*\***: Encargada de controlar los `usuarios` y` redes sociales`.

2. \***\*DATABASE\*\***: Administrador de la capa de datos, expone los endpoints necesarios para gestionar la base de datos.

3. \***\*CLIENTE\*\***: A cargo de servir las archivos estáticos, se implementó Angular para el desarrollo del cliente.

El [proyecto](http://3.128.197.28/) esta desplegado en EC2 - AWS con persistencia en Mongo Atlas

```
http://3.128.197.28/
```

La [documentación](http://3.128.197.28/api-docs/) del [proyecto](http://3.128.197.28/) esta en:

```
http://3.128.197.28/api-docs/
```

El archivos [`json`](http://3.128.197.28/Service%20database.postman_collection.json) para trabajar con Postman esta disponible en:

```
http://3.128.197.28/Service%20database.postman_collection.json
```

# Repositorio

Puede clonar el proyecto con:

```sh
$ git clone https://gitlab.com/giothcode/backend-nodejs-microservices.git task
```

Ingrese a la carpeta

```
$ cd task
```

Instale dependencias del proyecto

```sh
$ npm i
```

Configure variables de entorno

```sh

SRV_API_PORT=3000
SRV_DATABASE=3001
SRV_CLIENT_PORT=3002
SRV_DATABASE_HOST=localhost


MONGO_USER=
MONGO_PASSWORD=
MONGO_DB=
```

Puede gestionar los servicios con pm2

```sh
$ npm i -g pm2
```

```sh
$ pm2 start api/index.js
```

```sh
$ pm2 start database/index.js
```

```sh
$ pm2 start client/index.js
```

Finalmente puede verificar el estado:

```sh
$ pm2 status
```

Acceda a:

```sh
$ http://localhost:3002
```
