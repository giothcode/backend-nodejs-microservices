const express = require("express");
const cors = require("cors");
const swaggerUi = require("swagger-ui-express");
const swaggerDoc = require("../swagger.json");
const debug = require("debug")("service:api");
const path = require("path");

const {
  client: { port: PORT },
} = require("../config");
const { ErrorMiddleware } = require("../network/errors");

const app = express();

app.use(express.json());
app.use(cors());

app.use("/api-docs", swaggerUi.serve, swaggerUi.setup(swaggerDoc));

app.all("*", express.static(path.join(__dirname, "public")));

app.use(ErrorMiddleware);

app.listen(PORT, () => {
  debug(`Escuchando en el puerto ${PORT}`);
});
