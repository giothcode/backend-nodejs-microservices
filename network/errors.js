const response = require("./response");
const debug = require("debug")("service:error-handler");

module.exports.ErrorMiddleware = (err, req, res, next) => {
  debug("[Error]", err);

  if (err.statusCode) response.error(res, err.statusCode, err.message);
  else response.error(res, 500, "Internal Server Error");
};
