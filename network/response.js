exports.success = (res, statusCode = 200, message = "Ok", body = {}) => {
  res.status(statusCode).json({ error: false, message, body });
};

exports.error = (
  res,
  statusCode = 500,
  message = "Internal Server Error",
  body = {}
) => {
  res.status(statusCode).json({ error: true, message, body });
};
