const express = require("express");
const cors = require("cors");
const debug = require("debug")("service:api");

const {
  api: { port: PORT },
} = require("../config");
const { UsersNetwork } = require("./components/user/network");
const { SocialMediaNetwork } = require("./components/social-network/network");
const { ErrorMiddleware } = require("../network/errors");
const response = require("../network/response");

const app = express();

app.use(express.json());
app.use(cors());

app.use("/api/users", UsersNetwork);
app.use("/api/social-media", SocialMediaNetwork);
app.all("*", (req, res) => {
  response.success(res, 400, "Not found");
});
app.use(ErrorMiddleware);

app.listen(PORT, () => {
  debug(`Escuchando en el puerto ${PORT}`);
});
