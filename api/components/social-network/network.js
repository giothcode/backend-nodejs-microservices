const router = require("express").Router();

const response = require("../../../network/response");
const { Controller } = require("./index");
const {
  SchemaValidatorMiddleware,
  idSchema,
} = require("../../common/schemaValidator");
const { SocialMediaSchemas } = require("./schemas");

router
  .get("/", async (req, res, next) => {
    try {
      const socialNetworks = await Controller.list();
      response.success(res, 200, "Lista de redes sociales", socialNetworks);
    } catch (error) {
      next(error);
    }
  })
  .get(
    "/:id",
    SchemaValidatorMiddleware(idSchema, "params"),
    async (req, res, next) => {
      try {
        const {
          params: { id },
        } = req;
        const socialNetwork = await Controller.getById(id);
        if (socialNetwork) {
          response.success(
            res,
            200,
            `Red Social ${socialNetwork._id}`,
            socialNetwork
          );
        } else {
          response.error(res, 404, "Not Found");
        }
      } catch (error) {
        next(error);
      }
    }
  )
  .post(
    "/",
    SchemaValidatorMiddleware(SocialMediaSchemas.createSocialMedia),
    async (req, res, next) => {
      try {
        const { body } = req;
        const socialNetwork = await Controller.create(body);
        response.success(res, 200, `Red agregada`, socialNetwork);
      } catch (error) {
        next(error);
      }
    }
  )
  .patch(
    "/:id",
    SchemaValidatorMiddleware(idSchema, "params"),
    SchemaValidatorMiddleware(SocialMediaSchemas.updateSocialMedia),
    async (req, res, next) => {
      try {
        const {
          params: { id },
          body,
        } = req;
        const socialNetwork = await Controller.getById(id);
        if (socialNetwork) {
          const result = await Controller.update(socialNetwork._id, body);
          response.success(
            res,
            200,
            `Red ${socialNetwork._id} actualizado`,
            result
          );
        } else {
          response.error(res, 404, "Not Found");
        }
      } catch (error) {
        next(error);
      }
    }
  )
  .delete(
    "/:id",
    SchemaValidatorMiddleware(idSchema, "params"),
    async (req, res, next) => {
      try {
        const {
          params: { id },
        } = req;
        const socialNetwork = await Controller.getById(id);
        if (socialNetwork) {
          const result = await Controller.remove(id);
          response.success(
            res,
            200,
            `Red ${socialNetwork._id} eliminada`,
            result
          );
        } else {
          response.error(res, 404, "Not Found");
        }
      } catch (error) {
        next(error);
      }
    }
  );

module.exports.SocialMediaNetwork = router;
