const Joi = require("joi");

const createSocialMedia = Joi.object({
  userId: Joi.string()
    .regex(/^[0-9a-fA-F]{24}$/)
    .empty()
    .required(),
  name: Joi.string().empty().required(),
  username: Joi.string().empty().required(),
  link: Joi.string().empty().required(),
  icon: Joi.string().empty().required(),
});

const updateSocialMedia = Joi.object({
  name: Joi.string().empty().required(),
  username: Joi.string().empty().required(),
  link: Joi.string().empty().required(),
  icon: Joi.string().empty().required(),
});

module.exports.SocialMediaSchemas = {
  createSocialMedia,
  updateSocialMedia,
};
