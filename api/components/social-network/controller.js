const { Controller: UserController } = require("../user/index");

const COLLECTION = "social-network";

module.exports.SocialNetworkController = (store) => {
  const collection = store(COLLECTION);

  const list = async () => await collection.list();

  const getById = async (id) => await collection.getById(id);

  const create = async (socialMedia) => {
    const userId = socialMedia.userId;
    delete socialMedia.userId;
    const result = await collection.create(socialMedia);
    await UserController.addSocialMedia(userId, result.insertedId);
    return result;
  };
  const update = async (id, socialMedia) =>
    await collection.update(id, socialMedia);

  const remove = async (id) => {
    const user = await UserController.getBySocialMedia(id);
    await UserController.removeSocialMedia(user._id, id);
    return await collection.deleteDoc(id);
  };

  return { list, getById, create, update, remove };
};
