const router = require("express").Router();

const response = require("../../../network/response");
const {
  SchemaValidatorMiddleware,
  idSchema,
} = require("../../common/schemaValidator");
const { Controller } = require("./index");
const { UserSchemas } = require("./schemas");

router
  .get("/", async (req, res, next) => {
    try {
      const users = await Controller.list();
      response.success(res, 200, "Lista de usuarios", users);
    } catch (error) {
      next(error);
    }
  })
  .get(
    "/:id",
    SchemaValidatorMiddleware(idSchema, "params"),
    async (req, res, next) => {
      try {
        const {
          params: { id },
        } = req;
        const user = await Controller.getById(id);
        if (user) {
          response.success(res, 200, `Usuario ${user._id}`, user);
        } else {
          response.error(res, 404, "Not Found");
        }
      } catch (error) {
        next(error);
      }
    }
  )
  .post(
    "/",
    SchemaValidatorMiddleware(UserSchemas.createUser),
    async (req, res, next) => {
      try {
        const { body } = req;
        const user = await Controller.create(body);
        response.success(res, 200, `Usuario creado`, user);
      } catch (error) {
        next(error);
      }
    }
  )
  .patch(
    "/:id",
    SchemaValidatorMiddleware(idSchema, "params"),
    SchemaValidatorMiddleware(UserSchemas.updateUser),
    async (req, res, next) => {
      try {
        const {
          params: { id },
          body,
        } = req;
        const user = await Controller.getById(id);
        if (user) {
          const result = await Controller.update(user._id, body);
          response.success(res, 200, `Usuario ${user._id} actualizado`, result);
        } else {
          response.error(res, 404, "Not Found");
        }
      } catch (error) {
        next(error);
      }
    }
  )
  .delete(
    "/:id",
    SchemaValidatorMiddleware(idSchema, "params"),
    async (req, res, next) => {
      try {
        const {
          params: { id },
        } = req;
        const user = await Controller.getById(id);
        if (user) {
          const result = await Controller.remove(id);
          response.success(res, 200, `Usuario ${user._id} eliminado`, result);
        } else {
          response.error(res, 404, "Not Found");
        }
      } catch (error) {
        next(error);
      }
    }
  );

module.exports.UsersNetwork = router;
