const Joi = require("joi");

const createUser = Joi.object({
  firstname: Joi.string().empty().required(),
  lastname: Joi.string().empty().required(),
  email: Joi.string().email().empty().required(),
});

const updateUser = Joi.object({
  firstname: Joi.string().empty().optional(),
  lastname: Joi.string().empty().optional(),
  email: Joi.string().email().empty().optional(),
});

module.exports.UserSchemas = {
  createUser,
  updateUser,
};
