const COLLECTION = "users";

module.exports.UsersController = (store) => {
  const collection = store(COLLECTION);

  const list = async () =>
    await collection.aggregate([
      {
        $lookup: {
          from: "social-network",
          localField: "socialMedia._id",
          foreignField: "_id",
          as: "socialMedia",
        },
      },
    ]);

  const getById = async (id) => {
    let result = await collection.aggregate([
      { $match: { _id: id } },
      {
        $lookup: {
          from: "social-network",
          localField: "socialMedia._id",
          foreignField: "_id",
          as: "socialMedia",
        },
      },
    ]);
    return result.shift();
  };

  const create = async (user) => {
    user.socialMedia = [];
    return await collection.create(user);
  };

  const update = async (id, user) =>
    await collection.update(id, { $set: user });

  const addSocialMedia = async (id, socialMediaId) => {
    return await collection.update(id, {
      $push: {
        socialMedia: { _id: socialMediaId },
      },
    });
  };

  const removeSocialMedia = async (id, socialMediaId) => {
    return await collection.update(id, {
      $pull: {
        socialMedia: { _id: socialMediaId },
      },
    });
  };

  const remove = async (id) => {
    const user = await collection.getById(id);
    const ids = user.socialMedia.map((sm) => sm._id);
    await collection.deleteArray(ids);
    return await collection.deleteDoc(id);
  };

  const getBySocialMedia = async (socialMediaId) => {
    let result = await collection.aggregate([
      {
        $lookup: {
          from: "social-network",
          localField: "socialMedia._id",
          foreignField: "_id",
          as: "socialMedia",
        },
      },
      {
        $match: {
          "socialMedia._id": socialMediaId,
        },
      },
      {
        $project: {
          _id: 1,
        },
      },
    ]);
    return result.shift();
  };

  return {
    list,
    getById,
    create,
    update,
    remove,
    addSocialMedia,
    removeSocialMedia,
    getBySocialMedia,
  };
};
