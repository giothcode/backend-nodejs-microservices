const axios = require("axios");

const {
  database: { host: HOST, port: PORT },
} = require("../../config");

module.exports.DatabaseService = (collection) => {
  const URL = `http://${HOST}:${PORT}`;
  const http = axios.create({
    baseURL: `${URL}/${collection}`,
  });

  const list = async () => {
    const { data } = await http.get();
    return data.body;
  };

  const getById = async (id) => {
    const { data } = await http.get(id);
    return data.body;
  };

  const create = async (document) => {
    const { data } = await http.post("/", document);
    return data.body;
  };

  const update = async (id, query) => {
    const { data } = await http.patch(id, query);
    return data.body;
  };

  const deleteDoc = async (id) => {
    const { data } = await http.delete(id);
    return data.body;
  };

  const deleteArray = async (array) => {
    const { data } = await http.delete("/to/social-network", { data: array });
    return data.body;
  };

  const aggregate = async (aggregate) => {
    const { data } = await http.post("/aggregate", aggregate);
    return data.body;
  };

  return { list, getById, create, update, deleteDoc, aggregate, deleteArray };
};
