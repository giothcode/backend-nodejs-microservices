const Joi = require("joi");
const response = require("../../network/response");

const validate = (data, schema) => schema.validate(data);

module.exports.SchemaValidatorMiddleware =
  (schema, check = "body") =>
  (req, res, next) => {
    const { error, value } = validate(req[check], schema);
    if (error) {
      const { details } = error;
      response.error(res, 400, "Bad Request", { message: details[0].message });
    } else {
      req[check] = value;
      next();
    }
  };

module.exports.idSchema = Joi.object({
  id: Joi.string()
    .regex(/^[0-9a-fA-F]{24}$/)
    .empty()
    .required(),
});
