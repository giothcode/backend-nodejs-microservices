require("dotenv").config();

module.exports = {
  api: {
    port: process.env.SRV_API_PORT || 3000,
  },
  database: {
    host: process.env.SRV_DATABASE_HOST || "localhost",
    port: process.env.SRV_DATABASE || 3001,
    mongo: {
      uri: `mongodb+srv://${process.env.MONGO_USER}:${process.env.MONGO_PASSWORD}@backend.jo5gm.mongodb.net/${process.env.MONGO_DB}?retryWrites=true&w=majority`,
    },
  },
  client: {
    port: process.env.SRV_CLIENT_PORT || 3002,
  },
};
